Source: golang-connectrpc-connect
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Maytham Alsudany <maytha8thedev@gmail.com>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-google-go-cmp-dev,
               golang-google-protobuf-dev,
               golang-golang-x-net-dev,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-connectrpc-connect
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-connectrpc-connect.git
Homepage: https://github.com/connectrpc/connect-go
Rules-Requires-Root: no
XS-Go-Import-Path: connectrpc.com/connect

Package: golang-connectrpc-connect-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-google-go-cmp-dev,
         golang-google-protobuf-dev,
         golang-golang-x-net-dev,
         ${misc:Depends}
Description: Go implementation of Connect (library)
 Connect is a slim library for building browser and gRPC-compatible HTTP APIs.
 You write a short Protocol Buffer schema and implement your application logic,
 and Connect generates code to handle marshaling, routing, compression, and
 content type negotiation. It also generates an idiomatic, type-safe client.
 Handlers and clients support three protocols: gRPC, gRPC-Web, and Connect's own
 protocol.
 .
 The Connect protocol is a simple protocol that works over HTTP/1.1 or HTTP/2.
 It takes the best portions of gRPC and gRPC-Web, including streaming, and
 packages them into a protocol that works equally well in browsers, monoliths,
 and microservices. Calling a Connect API is as easy as using curl.

Package: protoc-gen-connect-go
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: protobuf-compiler,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Go implementation of Connect (protobuf plugin)
 Connect is a slim library for building browser and gRPC-compatible HTTP APIs.
 You write a short Protocol Buffer schema and implement your application logic,
 and Connect generates code to handle marshaling, routing, compression, and
 content type negotiation. It also generates an idiomatic, type-safe client.
 Handlers and clients support three protocols: gRPC, gRPC-Web, and Connect's own
 protocol.
 .
 The Connect protocol is a simple protocol that works over HTTP/1.1 or HTTP/2.
 It takes the best portions of gRPC and gRPC-Web, including streaming, and
 packages them into a protocol that works equally well in browsers, monoliths,
 and microservices. Calling a Connect API is as easy as using curl.
 .
 protoc-gen-connect-go is a plugin for the Protobuf compiler that generates Go
 code for Connect.
